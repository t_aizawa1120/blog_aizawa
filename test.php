<?php
$dsn = 'mysql:dbname=blog;host=localhost';
$user = 'root';
$password = '';

try {
    $dbh = new PDO($dsn, $user, $password);
    echo "接続成功\n";
    // SQL文を準備します。「:id」がプレースホルダーです。
    $sql = "SELECT * FROM article";
    // PDOStatementクラスのインスタンスを生成します。
    $prepare = $dbh->prepare($sql);

    $prepare->execute();

    // PDO::FETCH_ASSOCは、対応するカラム名にふられているものと同じキーを付けた 連想配列として取得します。
    $result = $prepare->fetchAll(PDO::FETCH_ASSOC);

    // 結果を出力
    var_dump($result);

} catch (PDOException $e) {
    echo "接続失敗: " . $e->getMessage() . "\n";
    exit();
}

?>