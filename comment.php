<?php

require_once("functions.php");

$art_id = "";

if(isset($_GET["art_id"])){
    $art_id = $_GET["art_id"];
}


class Validation{
    //入力されているかどうかのメソッド
    //入力された文字が短すぎないか、長すぎないか
    //メールアドレスかどうか

    //    // インスタンス化するときに引数を受け取れるようにしている
    //    public function __construct($引数) {
    //     $this->クラス内変数 = $引数;
    private $name;
    private $comment;
    private $error_text;
    #private $status;

    public function setName($name){
        $this->name = $name;
    }

    public function setComment($comment){
        $this->comment = $comment;
    }

    #$status = array([["$this->name", "タイトル", 10],["$this->comment", "本文", 30]]);

    public function checkTextEmpty(){
        if(strlen($this->name) == 0){
            $this->error_text .= "・名前が未入力です。<br>";
        }

        if(strlen($this->comment) == 0){
            $this->error_text .= "・コメントが未入力です。<br>";
        }
    }

    public function checkMaxName(){
        if(mb_strlen($this->name) > 10){
            $this->error_text .= "・名前が長すぎます(10文字以下)。<br>";
        }
        if(mb_strlen($this->comment) > 100){
            $this->error_text .= "・コメントが長すぎます(50文字以下)。<br>";
        }
    }

    public function errorText(){
        $this->checkTextEmpty();
        $this->checkMaxName();

        return $this->error_text;
    }

}

$error_text = "";
$name_text = "";
$comment_text = "";

if(isset($_POST["submit"])){
    $name = $_POST["name"];
    $comment = $_POST["comment"];
    $art_id = $_POST["art_id"];

    $vld = new Validation();

    $vld->setName($name);
    $vld->setComment($comment);
    $error_text = $vld->errorText();

    if($error_text == 0){
        $dbh = new PDO($dsn, $user, $password);
        // $st = $dbh->query("INSERT INTO `comment` (`id`, `art_id`, `name`, `comment`) 
        // VALUES (NULL, '{$art_id}','{$name}', '{$comment}') ");
        $st = $dbh->query("INSERT INTO `comment` (`id`, `art_id`, `name`, `comment`) VALUES (NULL, '{$art_id}', '{$name}', '{$comment}')");

        #メインページにリダイレクト
        header("Location: ./index.php");
        
    }else{
        $name_text = $name;
        $comment_text = $comment;
    }
}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>コメント投稿 | ブログ</title>
	<link rel="stylesheet" type="text/css" href="style.css">
    <style>
        span{
            color: red ;
        }
    </style>
</head>
<body>

	<main class="blog">

		<form action="./comment.php" method="post">
			<section class="article">
				<h2 class="article__title">コメント投稿</h2>
				
				<div class="article__post">
					<div class="article__postSet">
						<p>お名前</p>
						<p><input type="text" name="name" size="40" value=<?php echo $name_text; ?>></p>
					</div>

					<div class="article__postSet">
						<p>コメント</p>
						<p><textarea name="comment" row="8" cols="40"><?php echo $comment_text; ?></textarea></p>
					</div>

					<div class="article__postSet">
						<p>
							<input class="article__submit" name="submit" type="submit" value="投稿">
                            <input type="hidden" name="art_id" value=<?php echo $art_id ?>>
						</p>
		    		<p>
		    			<!-- ・名前を入力してください。<br>
		    			・テキストを入力してください。 -->
                        <span>
                        <?php
                        if(strlen($error_text)>0){
                            echo $error_text;
                        }
                        ?>
                        </span>
		    		</p>
		    	</div>
		    </div>

			</section>
		</form>
	</main>

</body>
</html>