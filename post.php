<?php
require_once("functions.php");

class Validation{
    //入力されているかどうかのメソッド
    //入力された文字が短すぎないか、長すぎないか
    //メールアドレスかどうか

    //    // インスタンス化するときに引数を受け取れるようにしている
    //    public function __construct($引数) {
    //     $this->クラス内変数 = $引数;
    private $title;
    private $contetnt;
    private $error_text;
    #private $status;

    public function setTitle($title){
        $this->title = $title;
    }

    public function setContent($content){
        $this->content = $content;
    }

    #$status = array([["$this->title", "タイトル", 10],["$this->content", "本文", 30]]);

    public function checkTextEmpty(){
        if(strlen($this->title) == 0){
            $this->error_text .= "・タイトルが未入力です。<br>";
        }

        if(strlen($this->content) == 0){
            $this->error_text .= "・本文が未入力です。<br>";
        }
    }

    public function checkMaxTitle(){
        if(mb_strlen($this->title) > 10){
            $this->error_text .= "・タイトルが長すぎます(10文字以下)。<br>";
        }
        if(mb_strlen($this->content) > 100){
            $this->error_text .= "・本文が長すぎます(50文字以下)。<br>";
        }
    }

    public function errorText(){
        $this->checkTextEmpty();
        $this->checkMaxTitle();

        return $this->error_text;
    }

}


$error_text = "";
$title_text = "";
$content_text = "";

if(isset($_POST["submit"])){
    // v($_FILES["image"]);

    // move_uploaded_file(一時的に保存されているファイル名,保存したいファイル名);

    // move_uploaded_file($_FILES["image"]["tmp_name"], "upload.jpg");

    $title = $_POST["title"];
    $content = $_POST["content"];

    $vld = new Validation();

    $vld->setTitle($title);
    $vld->setContent($content);
    $error_text = $vld->errorText();

    #move_uploaded_file($_FILES["image"]["tmp_name"], "upload.jpg");

    if(strlen($error_text)==0){
        $dbh = new PDO($dsn, $user, $password);
        $st = $dbh->query("INSERT INTO `article` (`id`, `title`, `main_text`, `date`) 
        VALUES (NULL, '{$title}', '{$content}', CURRENT_DATE())");

        // $id = $pdo->lastInsertId();
        // v($id);
        // exit;


        $id = $dbh->lastInsertId();
        // v($id);
        // exit;

        //issetだとファイルがなくても通る
        if($_FILES["image"]["size"] != 0){
            move_uploaded_file($_FILES["image"]["tmp_name"], "./up_img/img_$id.jpg");
        }

        #メインページにリダイレクト
        header("Location: ./index.php");
        
    }else{
        $title_text = $title;
        $content_text = $content;
    }
}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>ブログ</title>
	<link rel="stylesheet" type="text/css" href="style.css">
    <style>
        span{
            color: red ;
        }
    </style>
</head>
<body>

	<main class="blog">

		<form action="post.php" method="post" enctype="multipart/form-data">
			<section class="article">
				<h2 class="article__title">記事投稿</h2>
				
				<div class="article__post">
					<div class="article__postSet">
						<p>題名</p>
						<p><input type="text" name="title" size="40" value=<?php echo $title_text; ?>></p>
					</div>

					<div class="article__postSet">
						<p>本文</p>
						<p><textarea name="content" row="8" cols="40"><?php echo $content_text; ?></textarea></p>
					</div>

                    <div class="article__postSet">
                        <p>画像</p>
                        <p><input type="file" name="image"></p>
                    </div>

					<div class="article__postSet">
						<p><input class="article__submit" name="submit" type="submit" value="投稿"></p>
		    		<p>
		    			<!-- ・名前を入力してください。<br>
		    			・テキストを入力してください。 -->
                        <span>
                        <?php
                        if(strlen($error_text)>0){
                            echo $error_text;
                        }
                        ?>
                        </span>
		    		</p>
		    	</div>
		    </div>

			</section>
		</form>
	</main>

</body>
</html>